/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef MOCK_PRINT_SERVICE_STUB_H
#define MOCK_PRINT_SERVICE_STUB_H

#include <gmock/gmock.h>

#include "print_service_stub.h"

namespace OHOS {
namespace Print {
class MockPrintServiceStub final : public PrintServiceStub {
public:
    MOCK_METHOD3(StartPrint, int32_t(const std::vector<std::string> &fileList,
    const std::vector<uint32_t> &fdList, std::string &taskId));
};
} // namespace Print
} // namespace OHOS
#endif // MOCK_PRINT_SERVICE_STUB_H